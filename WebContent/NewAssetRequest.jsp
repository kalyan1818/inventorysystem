<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, org.inventory.dao.*, org.inventory.jdo.*" %>

<html>
<jsp:include page="Header.jsp">
   	<jsp:param value="test" name="test"/>
</jsp:include>
<body class="bg-steel">
	<div style="width:85%; margin:0 auto;">
    <jsp:include page="Menus.jsp">
    	<jsp:param value="test" name="test"/>
	</jsp:include>
    <div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">

<%
	String action = request.getParameter("action");
	System.out.println("action = "+action);
	if(action == null)
	{
%>
		<div class="row"> <!-- style="height: 100%" -->
		    <div class="cell auto-size padding20 bg-white" id="cell-content">
		        <h3 class="text-light"> New Asset Request </h3>
		        <hr class="thin bg-grayLighter">
		        <form method="post" action="./NewAssetRequest.jsp" data-role="validator">
		        <input type="hidden" name="action" id="action" value="save" />
		        <table class="dataTable border bordered" data-auto-width="false" style="width:80%;">
		            <tbody>
		            <tr>
		                <td>Request For</td>
		                <td>
		                	<div class="input-control text" style="height: 30px;">
								<select id="request_for" name="request_for">
									<option>Laptop</option>
									<option>External Hard Disk</option>
								</select>
		    				</div>
		 				</td>
			 		</tr>
			 		<tr>
			 			<td>Duration</td>
		                <td style="height: 30px; width:150px;">
		                 	<select id="duration" name="duration">
								<option>6 Months</option>
								<option>1 Year</option>
								<option>2 Years</option>
								<option>3 Years</option>
							</select>
			  			</td>
		             </tr>
		             </tbody>
		         </table>
		         <div style="padding-left: 35%;">
		        	<button class="button primary" > Submit </button>
		        </div>
		        </form>
		    </div>
		</div>
<%
	}
	else
	{
%>
		<div class="row"> <!-- style="height: 100%" -->
		    <div class="cell auto-size padding20 bg-white" id="cell-content">
		        <h3 class="text-light">
<%
		//System.out.println("action = "+action);
		String request_for = request.getParameter("request_for");
		String duration = request.getParameter("duration");
		String userName = (String)session.getAttribute("username");
		
		boolean status = AssetRequestDao.saveAssetRequest(request_for, 
			duration, userName);
		if(status)
		{
			out.println("Request submitted successfully.");
		}
		else
		{
			out.println("Error submitting Request;"); 
			out.println("Please contact System Admin or try again later.");
		}
%>
				</h3>
		        <hr class="thin bg-grayLighter">
		    </div>
		</div>
<%
	}
%>
        </div>
    </div>
    </div>
</body>
</html>